{
	'name': 'Isha Rejuvenation',
	'summary': "Isha Rejuvenation is about rekindling the fundamental force of creation within you, to change the very chemistry of who you are. - Sadhguru", 
	'description': "Isha Rejuvenation Center has been established by Sadhguru to allow an individual to experience inner peace and the joy of a healthy body through a unique and powerful set of offerings. These include residential programs, walk-in medical consultations and Ayurvedic therapies. The Rejuvenation Center is located in the rich, salubrious environs of the Isha Yoga Center at the foothills of the Velliangiri Mountains. These mountains are referred to as the Kailash of the South – they have a powerful spiritual reverberation and also possess a natural beauty that is breathtaking. The Isha Yoga Center is a consecrated space and home to the Dhyanalinga, a unique meditative space that embodies the distilled essence of yogic sciences. The energies of the Dhyanalinga create an atmosphere that supports and catalyzes the rejuvenation process in the human system. Simply being in this space has profound transformative effects.", 
	'author': "Isha Foundation", 
	'license': "AGPL-3",
	'website': "https://isha.sadhguru.org/in/en/health/walk-in-medical-consultation/isha-rejuvenation-center",
	'category': 'Yoga & Meditation', 
	'version': '20.0.0.1', 
	'depends': ['base'], 
	'data': 
	[
		'views/views.xml',
		'views/programtype_view.xml',
		'views/programregistration_view.xml',
		'security/ir.model.access.csv',
		'security/security.xml',
		'menus/menus.xml'
	], 
	'demo': ['demo/demo.xml'],
	'application': True,
	'auto_install': False,
	'installable': True,
}
