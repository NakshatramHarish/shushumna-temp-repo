from odoo import models, fields, api, exceptions
from . import commonmodels

class ProgramType(models.Model): 
	_name = 'program.type'
	_description = 'Program Type'
	_order = 'programtype'
	_rec_name = 'programtype'
		
	lob = fields.Char(string = 'LOB', size=100, required=True, default='Rejuvenation')
	programtype = fields.Char(string = 'Program Type', size=100, required=True)
	active = fields.Boolean('Active', default=True)
	workflowpattern = fields.Selection([('Rejuvenation','Rejuvenation'), ('Retreat','Retreat'), ('Adv Pgms','Adv Pgms'), ('IE','IE'), ('IYP','IYP')], string='Workflow Pattern', default='Rejuvenation', size=100, required=True)
	numberofdays = fields.Integer(string='Number Of Days', size=50, required=True)
	isnri = fields.Boolean('Non-Resident?', help = "Default is false (it is resident), for NRI select this checkbox", default=False)
	abbrevation = fields.Char(string = 'Abbrevation', size=100, required=True)
	description = fields.Text('Description', size=500, required=True)
	copyconfigparamfrom = fields.Many2one('program.type', string = 'Copy config parameters from', size=500)
	
	# finance parameters	
	entity = fields.Many2one('rejuvenation.entity', string='Entity')
	entityisoverridable = fields.Boolean('Is Overridable', default=False)
	pgmpurpose = fields.Char(string = 'Pgm Purpose', size=100, required=True)
	pgmpurposeisoverridable = fields.Boolean('Is Overridable', default=False)
	paymentgateway =  fields.Many2one('rejuvenation.paymentgateway', string='Payment Gateway')
	paymentgatewayisoverridable = fields.Boolean('Is Overridable', default=False)
	
	# eligibility parameters
	minimumage = fields.Integer(string='Minimum Age', required=True)
	minimumageisoverridable = fields.Boolean('Is Overridable', default=False)
	gender = fields.Selection([('Male','Male'), ('Female','Female'), ('All','All')], string='Gender', required=True)
	genderisoverridable = fields.Boolean('Is Overridable', default=False)	
	language1 =  fields.Many2one('rejuvenation.language1', string='language')
	languageisoverridable = fields.Boolean('Is Overridable', default=False)
	prerequisitepractice = fields.Many2many('rejuvenation.practice', string='Prerequisite Practice')
	prerequisitepracticeisoverridable = fields.Boolean('Is Overridable', default=False)
	repetitionallowed = fields.Integer(string='Repetition Allowed', required=True)
	repetitionallowedisoverridable = fields.Boolean('Is Overridable', default=False)
	gapbetweenprgm = fields.Integer(string='Gap between two pgms', required=True)
	gapbetweenprgmisoverridable = fields.Boolean('Is Overridable', default=False)

	#package parameters
	packageparameters = fields.One2many('rejuvenation.package','packagecode', string="Package Parameters")

	# medical parameters (Health reports)
	healthreportsneededflag = fields.Boolean(string="Is Health Reports Needed",default=False)
	healthreportsneeded = fields.Selection([('Yes','Yes'), ('No','No')], string='Health Reports Needed?', required=True)
	healthreports =  fields.Many2many('rejuvenation.healthreports', string='Health Reports')
	healthreportsmandatory = fields.Selection([('Yes','Yes'), ('No','No')], string='Health Reports Mandatory?', required=True)
	doctoremail = fields.Char(string = 'Doctor Email', size=100, required=True)
	registrationsupport  = fields.Char(string = 'Registration Support', size=100, required=True)
	sendautoemailtodr  = fields.Selection([('Yes','Yes'), ('No','No')], string='Send auto email to Dr', required=True)


	# medical parameters 2
	enquiry = fields.Char(string = 'Enquiry', size=100, required=True)
	registration = fields.Char(string = 'Registration', size=100, required=True)
	medicalreports = fields.Char(string = 'Medical Reports', size=100, required=True)
	medicalassessment = fields.Char(string = 'Medical Assessment', size=100, required=True)
	confirmation = fields.Char(string = 'Confirmation', size=100, required=True)
	cancellationapproval = fields.Char(string = 'Cancellation Approval', size=100, required=True)
	refundprocessed = fields.Char(string = 'Refund Processed', size=100, required=True)
	rejection = fields.Char(string = 'Rejection', size=100, required=True)

	enquirysms = fields.Boolean(' ', default=False)	
	registrationsms = fields.Boolean(' ', default=False)	
	medicalreportssms = fields.Boolean(' ', default=False)	
	medicalassessmentsms = fields.Boolean(' ', default=False)	
	confirmationsms = fields.Boolean(' ', default=False)	
	cancellationapprovalsms = fields.Boolean(' ', default=False)	
	refundprocessedsms = fields.Boolean(' ', default=False)	
	rejectionsms = fields.Boolean(' ', default=False)	

	enquiryemail = fields.Boolean(' ', default=False)	
	registrationemail = fields.Boolean(' ', default=False)	
	medicalreportsemail = fields.Boolean(' ', default=False)	
	medicalassessmentemail = fields.Boolean(' ', default=False)	
	confirmationemail = fields.Boolean(' ', default=False)	
	cancellationapprovalemail = fields.Boolean(' ', default=False)	
	refundprocessedemail = fields.Boolean(' ', default=False)	
	rejectionemail = fields.Boolean(' ', default=False)	

	# notification template
	notificationtemplates = fields.One2many('rejuvenation.notificationtemplate', 'notificationevent', string = 'Notification Templates')


	#cancellation rules
	cancellationnoofdays1 = fields.Integer(string='Cancellation Rule', required=True)
	cancellationnoofdays2 = fields.Integer(string=' ', required=True)
	cancellationnoofdays3 = fields.Integer(string=' ', required=True)
	cancellationnoofdays1percent = fields.Integer(string=' ', required=True)
	cancellationnoofdays2percent = fields.Integer(string=' ', required=True)
	cancellationnoofdays3percent = fields.Integer(string=' ', required=True)
	participantreplacement = fields.Integer(string='Participance Replacement', required=True)

	
	# basic validations

	@api.constrains('numberofdays')
	def	validate_numberofdays(self):
			if (self.numberofdays <= 0 or self.numberofdays > 99):
					raise exceptions.ValidationError("Field 'Number of Days' should be between 1 to 99")

	@api.constrains('minimumage')
	def	validate_minimumage(self):
			if (self.minimumage < 15 or self.minimumage > 120):
					raise exceptions.ValidationError("Field 'Minimum Age' should be between 15 to 120")

	@api.constrains('repetitionallowed')
	def	validate_repetitionallowed(self):
			if (self.repetitionallowed < 0 or self.repetitionallowed > 999):
					raise exceptions.ValidationError("Field 'Repetition Allowed' should be between 1 to 999")

	@api.constrains('gapbetweenprgm')
	def	validate_gapbetweenprgm(self):
			if (self.gapbetweenprgm < 0 or self.gapbetweenprgm > 999):
					raise exceptions.ValidationError("Field 'Gap between two pgms' should be between 1 to 999")

	@api.constrains('cancellationnoofdays1')
	def	validate_cancellationnoofdays1(self):
			if (self.cancellationnoofdays1 < 0 or self.cancellationnoofdays1 > 999):
					raise exceptions.ValidationError("Field 'Cancellation Number Of Days Rule 1' should be between 1 to 999")				

	@api.constrains('cancellationnoofdays2')
	def	validate_cancellationnoofdays2(self):
			if (self.cancellationnoofdays2 < 0 or self.cancellationnoofdays2 > 999):
					raise exceptions.ValidationError("Field 'Cancellation Number Of Days Rule 2' should be between 1 to 999")				

	@api.constrains('cancellationnoofdays1')
	def	validate_cancellationnoofdays3(self):
			if (self.cancellationnoofdays3 < 0 or self.cancellationnoofdays3 > 999):
					raise exceptions.ValidationError("Field 'Cancellation Number Of Days Rule 3' should be between 1 to 999")				


	@api.constrains('cancellationnoofdays1percent')
	def	validate_cancellationnoofdays1percent(self):
			if (self.cancellationnoofdays1percent < 0 or self.cancellationnoofdays1percent > 100):
					raise exceptions.ValidationError("Field 'Cancellation Rule 1 percentage' should be between 1 to 100")				


	@api.constrains('cancellationnoofdays2percent')
	def	validate_cancellationnoofdays2percent(self):
			if (self.cancellationnoofdays2percent < 0 or self.cancellationnoofdays2percent > 100):
					raise exceptions.ValidationError("Field 'Cancellation Rule 2 percentage' should be between 1 to 100")				


	@api.constrains('cancellationnoofdays3percent')
	def	validate_cancellationnoofdays3percent(self):
			if (self.cancellationnoofdays3percent < 0 or self.cancellationnoofdays3percent > 100):
					raise exceptions.ValidationError("Field 'Cancellation Rule 13 percentage' should be between 1 to 100")


	@api.constrains('participantreplacement')
	def	validate_participantreplacement(self):
			if (self.participantreplacement < 0 or self.participantreplacement > 100):
					raise exceptions.ValidationError("Field 'Participant percentage ' should be between 1 to 100")























	@api.onchange('healthreportsneeded')
	def healthreportsneeded_change(self):
		if (self.healthreportsneeded == "Yes"):
			self.healthreportsneededflag = True
		else:
			self.healthreportsneededflag = False


	