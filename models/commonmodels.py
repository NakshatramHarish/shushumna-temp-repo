from odoo import models, fields, api, exceptions

class Entity(models.Model): 
	_name = 'rejuvenation.entity'
	_description = 'Entity'
	_order = 'entityname'
	_rec_name = 'entityname'

	entityname = fields.Char(string = 'Entity Name', size=100, required=True)

class PaymentGateway(models.Model): 
	_name = 'rejuvenation.paymentgateway'
	_description = 'Payment Gateway'
	_order = 'paymentgatewayname'
	_rec_name = 'paymentgatewayname'

	paymentgatewayname = fields.Char(string = 'Payment Gateway', size=100, required=True)	

class RejuvenationLanguage(models.Model): 
	_name = 'rejuvenation.language1'
	_description = 'Language Description'
	_order = 'languagedescription'
	_rec_name = 'languagedescription'

	languagedescription = fields.Char(string = 'Language Description', size=100, required=True)	

class PracticeMaster(models.Model): 
	_name = 'rejuvenation.practice'
	_description = 'Practice Master'
	_order = 'practicename'
	_rec_name = 'practicename'

	practicename = fields.Char(string = 'Practice Name', size=100, required=True)		

class HealthReportsMaster(models.Model): 
	_name = 'rejuvenation.healthreports'
	_description = 'Health Report Master'
	_order = 'healthreportname'
	_rec_name = 'healthreportname'

	healthreportname = fields.Char(string = 'Health Report Name', size=100, required=True)			

class PackageMaster(models.Model): 
	_name = 'rejuvenation.package'
	_description = 'Package'
	#_order = 'practicename'
	_rec_name = 'packagecode'

	# package parameters
	packagecode = fields.Char(string = 'Package Code', size=100, required=True)
	packagedisplaycode = fields.Char(string = 'Display Name', size=100, required=True)
	packagenoofseats = fields.Integer(string = 'No of Seats', size=50, required=True)
	packageemergencyseats = fields.Integer(string = 'Emergency Seats', required=True)
	packagetotalseats = fields.Integer(string = 'Total Seats',  required=True)
	packagecost = fields.Integer(string = 'Cost',  required=True)

	@api.constrains('packagenoofseats')
	def	validate_packagenoofseats(self):
		if (self.packagenoofseats <= 0 or self.packagenoofseats > 99):
			raise exceptions.ValidationError("Field 'Number of Seats' should be between 1 to 99")

	@api.constrains('packageemergencyseats')
	def	validate_packageemergencyseats(self):
		if (self.packageemergencyseats <= 0 or self.packageemergencyseats > 99):
			raise exceptions.ValidationError("Field 'Emergency Number of Seats' should be between 1 to 99")	

	@api.constrains('packagetotalseats')
	def	validate_packagetotalseats(self):
		if (self.packagetotalseats <= 0 or self.packagetotalseats > 99):
			raise exceptions.ValidationError("Field 'Total Number of Seats' should be between 1 to 99")		

	@api.constrains('packagecost')
	def	validate_packagecost(self):
		if (self.packagecost <= 0 or self.packagecost > 999999999):
			raise exceptions.ValidationError("Field 'Package cost' should be between 1 to 999999999")		


class NotificationMaster(models.Model): 
	_name = 'rejuvenation.notificationevent'
	_description = 'Notification Event'
	#_order = 'lob, programtype'
	_rec_name = 'notificationevent'

	notificationevent = fields.Char(string = 'Notification Event', size=100, required=True)


class NotificationTemplateMaster(models.Model): 
	_name = 'rejuvenation.notificationtemplate'
	_description = 'Notification Template'
	#_order = 'lob, programtype'
	_rec_name = 'notificationevent'

	notificationevent = fields.Many2one('rejuvenation.notificationevent', string = 'Notification Event')
	sendemail = fields.Boolean(string='Send Email', default=False)
	emailtemplateid = fields.Many2one('mail.template',string='Email Template',domain=[('model','=','program.type')],track_visibility='onchange')
	sendsms = fields.Boolean(string='Send SMS', default=False)
	smstemplateid = fields.Many2one('sms.template',string='SMS Template',domain=[('model','=','program.type')],track_visibility='onchange')