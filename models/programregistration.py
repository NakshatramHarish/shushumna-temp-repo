from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from . import commonmodels


class ValidationError(except_orm):
    """Violation of python constraints.

                 .. admonition:: Example

                     When you try to create a new user with a login which already exist in the db.
                 """

    def __init__(self, msg):
        super(ValidationError, self).__init__(msg)

class ProgramRegistration(models.Model):
    _name = 'program.registration'
    _description = 'Program Registration Forms'

    first_name = fields.Char(String="First Name", required=True)
    last_name = fields.Char(String="Last Name", required=True)
    name_called = fields.Char(String="Name you would like to be called")
    gender = fields.Selection([('Male', 'Male'), ('Female', 'Female'), ('All', 'All')], string='Gender')
    marital_status = fields.Selection(
        [('Single', 'Single'), ('Partnered', 'Partnered'), ('Married', 'Married'), ('Divorced', 'Divorced'),
         ('Widowed', 'Widowed')], string='Marital Status')
    dob = fields.Date(string='Date Of Birth', required=False)
    phone = fields.Char(string='Mobile Number', required=False, index=True)
    state = fields.Many2one('res.country.state', 'State/province')
    country = fields.Many2one('res.country', 'Country of residence')
    pincode = fields.Char(string="Pincode", required=False, index=True)
    address_line = fields.Char(string='Address Line', required=False)
    city = fields.Char(string='District/City', required=False)
    education_qualification = fields.Char(String="Educational Qualification")
    nationality_id = fields.Many2one('res.country', 'Nationality Id')
    occupation = fields.Char(string='Occupation', required=False)
    proof_ids = fields.One2many('idproof.attachment', 'idproof_id', string="ID Proof")
    name1 = fields.Char(String="Name 1", required=True)
    relationship1 = fields.Char(String="Relationship 1", required=True)
    phone1 = fields.Char(string='Mobile Number 1', required=False, index=True)
    name2 = fields.Char(String="Name 2", required=True)
    relationship2 = fields.Char(String="Relationship 2", required=True)
    phone2 = fields.Char(string='Mobile Number 2', required=False, index=True)


    class idproof_attach(models.Model):
        _name = "idproof.attachment"
        _description = "ID Proof Attachment"

        idproof_id = fields.Many2one('res.partner', string='idproof_attach')
        attached_file = fields.Many2many(comodel_name="ir.attachment",
                                         relation="m2m_ir_attached_file_rel",
                                         column1="m2m_id", column2="attachment_id", string="File Attachment",
                                         required=True)
        idproof_document_type = fields.Many2one('documenttype', string="ID Type", required=True)
        # Removing as per the UAT feedback
        # idproof_file = fields.Char(string="File Name", required=True)
        active = fields.Boolean('Active', default=True)

        #    state = fields.Boolean('State', default=True)

        @api.onchange('attached_file')
        def limit_attached_file(self):
            attachement = len(self.attached_file)
            if attachement > 1:
                raise ValidationError(_('You can add only 2 files per ID Proof'))
